# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Mis012 <Michael.Srba@seznam.cz>
# Co-Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=device-samsung-a3ulte
pkgdesc="Samsung Galaxy A3 (SM-A300FU)"
pkgver=4
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo rootston.ini"
subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem:kernel_mainline_modem
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem:nonfree_firmware_modem
	$pkgname-phosh
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem() {
	pkgdesc="Close to mainline kernel (non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/WiFi/BT/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-samsung-a3ulte-venus firmware-samsung-a3ulte-wcnss"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem() {
	pkgdesc="Modem firmware"
	depends="firmware-samsung-a3ulte-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem"
	mkdir "$subpkgdir"
}

phosh() {
	install_if="$pkgname postmarketos-ui-phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

sha512sums="09a25591bbc6cc2d78cb7d92aa0be42cfc629c6c6f98321eee6c0f710f26af5c652bb6e42e7070ab75d44845f0d34460f4c0b66ddc9ad6ae8f58071cb0d35477  deviceinfo
94c866b6583faadc4a96a4d737983ba7838ede52afa5e29e261ef0ad0f2afe29fd3b793c9208ae74c7d48db6b991ad21800b9e457fbba4c69ce9dec2cda268ea  rootston.ini"
